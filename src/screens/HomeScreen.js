import React, { useState } from 'react'
import { Text, View, StyleSheet, Linking, Dimensions, ScrollView } from 'react-native'

import email from 'react-native-email'

import Colors from '../styles/Colors'
import Input from '../components/input/Input'
import Toast from '../components/toast/Toast';
import Button from '../components/button/Button';

const width = Dimensions.get('window').width;

const HomeScreen = () => {

    const [userName, setUserName] = useState('')
    const [userNumber, setUserNumber] = useState('')
    const [userEmail, setUserEmail] = useState('')
    const [message, setMessage] = useState('')
    const [errorMsg, setErrorMsg] = useState('')


    const sendEmail = () => {
        console.log("send email");

        if(userName){
            if(userNumber){
                if(userEmail){
                    if(message){
                        var messageBody = message + "\n" + "\n" + "From"  + "\n" + userName + "\n" + userEmail + "\n" + userNumber
                        const sendEmailTo = 'info@redpositive.in'
                        const subject = 'Message to RedPositive'
                        Linking.openURL(`mailto:${sendEmailTo}?subject=${subject}&body=${messageBody}`)
                       
                    }else{
                        console.log("Enter the Message!");
                        setErrorMsg('Enter the Message!')
                    }
                }else{
                    console.log("Enter the Email!");
                    setErrorMsg('Enter the Email!')
                }
            }else{
                console.log("Enter the Number!");
                setErrorMsg('Enter the Number!')
            }
        }else{
            console.log("Enter the Name!");
            setErrorMsg('Enter the Name!')
        }  
    }

    return (
        <View style={styles.mainContainer} >
            <ScrollView>
                <View style={{alignItems: 'center'}} >
                    <Text style={styles.contactUs} >Contact Us</Text>
                </View>

                <View style={{marginBottom: 80}} >
                    <Input
                        value={userName}
                        placeholder="Full Name"
                        onChange={(text) => {setUserName(text); setErrorMsg('');}}
                    />

                    <Input
                        value={userNumber}
                        keyboard="numeric"
                        placeholder="Mobile Number"
                        onChange={(text) => {setUserNumber(text); setErrorMsg('');}}
                    />

                    <Input
                        value={userEmail}
                        placeholder="Email"
                        onChange={(text) => {setUserEmail(text); setErrorMsg('');}}
                    />

                    <Input
                        value={message}
                        placeholder="Message"
                        messageInput={true}
                        onChange={(text) => {setMessage(text); setErrorMsg('');}}
                    />
                </View>
                
            </ScrollView>
           
            <View style={{position: 'absolute', width: width, alignItems: 'center', bottom: 20, }} >
                {errorMsg ? <Toast value={errorMsg} /> : null }
                <Button title="Contact Us" onPress={sendEmail} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        padding: 20,
        backgroundColor: Colors.BACKGROUND
    },
    contactUs: {
        fontSize: 26,
        fontWeight: 'bold',
        color: Colors.BROWN
    }
})

export default HomeScreen