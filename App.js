import React, { useEffect, useState } from 'react'
import { Text, View, StatusBar } from 'react-native'
import HomeScreen from './src/screens/HomeScreen';

const App = () => {

    return (
        <>
            <StatusBar backgroundColor="#000" barStyle="light-content" />
            <HomeScreen/>
        </>
    )
}

export default App